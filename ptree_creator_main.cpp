/*by Erik Wannerberg
 - program to view / create and traverse ptrees -
 24/7 - 2013
 */

#include <iostream>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

void jsonPrinter(boost::property_tree::ptree ptOut, std::string saveFilePath);
void treePopulator(boost::property_tree::ptree &ptChange);
void jsonReader(boost::property_tree::ptree &ptIn, std::string saveFilePath);
void jsonTerminalPrinter(boost::property_tree::ptree ptOut);

int main(){

	std::string filePath;

	std::cout << "Enter filename/path for the tree you want to load/write" << std::endl;
	std::cin >> filePath;

	boost::property_tree::ptree myTree;

	jsonReader(myTree, filePath);
	std::cout << "Enter filename to save to" << std::endl;
	std::string saveFilePath;
	std::cin >> saveFilePath;

	if (saveFilePath == "terminal"){		// print to terminal
		jsonTerminalPrinter(myTree);
	}
	else {
	}

	jsonPrinter(myTree,saveFilePath);


//	myTree.put_value("central node");


	std::cout << "success" <<std::endl;

	return 0;



}

void jsonPrinter(boost::property_tree::ptree ptOut, std::string saveFilePath) {
	boost::property_tree::json_parser::write_json(saveFilePath, ptOut);
}
void treePopulator(boost::property_tree::ptree &ptChange) {
	ptChange.put("child","hasSumthing");
	ptChange.put("groove.structure.cat","meow");
	ptChange.put("groove.structure.head","woot");
	ptChange.put("groove.structure.cucumber","i'm green");
	ptChange.add("groove.structure.cat","meow!!!");
}

void jsonReader(boost::property_tree::ptree &ptIn, std::string saveFilePath) {
	boost::property_tree::json_parser::read_json(saveFilePath, ptIn);
}

void jsonTerminalPrinter(boost::property_tree::ptree ptOut){
	boost::property_tree::json_parser::write_json(std::cout, ptOut);

}
